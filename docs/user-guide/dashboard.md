# Dashboard

![Screenshot](../img/dashboard.jpg)

## 1. Tickect Pendientes

Listado de tickets pendientes indicando la cantidad

![Screenshot](../img/tk_pendientes.jpg)

## 2. Tickect esta semana

Cantidad de atenciones desde el primer dia la semana `Lunes`.

## 3. Tickect este mes

Cantidad de atenciones desde el primer dia del mes.

## 4. Fecha y Hora

Fecha y hora del equipo del usuario.

## 5. Dashboard de Kpis e indicadores

Resumen de indicadores y Kpis de gestion.

## 6. Salir del sistema

![Screenshot](../img/logout.jpg)

## 7. Opciones de usuario

![Screenshot](../img/user.jpg)

### A. Información de Usuario

Permite actualizar la información personal del usuario.

![Screenshot](../img/info_user.jpg)

### B. Cambiar password

Permite que el usuario cambie su password cuando lo desee.

![Screenshot](../img/change_password.jpg)

## 8. Menu de Opciones

![Screenshot](../img/menu.jpg)