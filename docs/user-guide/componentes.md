# Componentes

![Screenshot](../img/menu_components.jpg)

## 1. Detalle

Lista los `componentes` de todos los sites, haciendo click en cada una de las cabeceras se pueden ordenar los datos de forma ascendente o descente, se ser el caso se puede filtrar los datos([ver sección 7](#7-filtros-personalizados)).

![Screenshot](../img/det_components.jpg)

## 2. `Nuevo` - Registrar Componente

Esta opción permite registar un nuevo `componente` asociado a un `Site`(proyecto/faena).

![Screenshot](../img/com_registrar.jpg)

**A. Site:** presenta opciones para seleccionar el site (proyecto/faena) al que pertenece el componente.

**B. Tipo:** presenta opciones para seleccionar el tipo(familia) de componte.

- GPS HP
- HUB-UHP-HP
- Pantalla 8.4"
- GPS LP
- Pantalla 12.1"
- Antena Wireles
- HUB-UHP-LP
- Switch Oring
- Inclinometro
- WINSYS-LP
- WINSYS-HP
- Inclinometro
- Profundimetro


**C. Serie:** Serie del componente (el sistema controla que las series no se dupliquen).

![Screenshot](../img/com_double_serie.jpg)

**D. Estado:**

- En Revisión
- Operativo
- Malogrado

**E. System:** representa la arquitectura del componente.

- UHP-HP
- UHP-LG
- TODO
- WINSYS


**F. Comentarios:** descripción adicional del componente

## 3. `Editar` - Editar Componente

Primero debemos seleccionar la casilla del `componente` que deseamos modificar, luego hacemos click en `Editar`.

![Screenshot](../img/com_editar.jpg)

Luego tenemos las mismas opciones que al `Registrar Componente`

![Screenshot](../img/com_update.jpg)

## 4. `Eliminar` - Eliminar Componentes

Primero debemos seleccionar una o varias casillas de los `componentes` que deseamos eliminar, luego hacemos click en `Eliminar`.

![Screenshot](../img/com_delete.jpg)

Confirmamos o cancelamos la eliminación.

![Screenshot](../img/com_delete_ok.jpg)

## 5. `Desmontar` - Desmontar componente

Esta acción permite acceder directamente a `desmontar` un componete asociado a un equipo(ubicación.)

Para ejecutar esta acción primero debemos seleccionar un componente y luego hacer click en  `desmontar`.

![Screenshot](../img/com_dismount_select.jpg)

Seguido se nos mostrara la información relevante(`A` y `B`) y también debemos ingresar información de la transacción(`C`). 

![Screenshot](../img/com_dismount_ok.jpg)

### A. Dispositivo

Muestra información del dispositivo(equipo) el que se encuentra montado(instalado) el componente actualmente, el site al que pertenece este y su tipo.

### B. Componente

Información del componente a desmontarse: Serie y tipo.

### C. Transación

1. `Responsable`: o responsables que realizaron desmontaron(desinstalaron) el componente.
2. `Motivo`: por el cual el componente es desmontado(desinstalaron).
   
    - Falla
    - Mantenimiento Mayor
    - Accidente
    - Disponibilidad de Componentes: cuando se desmontan componentes de un equipo de menor prioridad para montarlos en otro de mayor prioridad.
    - Equipo Fuera de Plan
    - Equipo de Baja

3. `Fecha`: fecha y hora en la que fue desmontado el componente.
4. `Comentarios`: información adicional.

## 6. `Ver transacciones` - Transacciones de componente

Esta opción permite visualizar el detalle de transacciones(`ciclo de vida`) de un componente, para visualizar este detalle primero debemos seleccionar el componente y seguido hacer click en `Ver transacciones`.

![Screenshot](../img/com_select_tran.jpg)

`Ciclo de vida` de un componente.

![Screenshot](../img/com_det_tran.jpg)

## 7. Filtros Personalizados

Todas las columnas cuentan con filtros personalizados, estos tambien pueden combinarse.

![Screenshot](../img/com_filter.jpg)