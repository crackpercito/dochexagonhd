# Devices

![Screenshot](../img/menu_devices.jpg)

## 1. Detalle

Lista todos los `devices`(equipos) de todos los sites, haciendo click en cada una de las cabeceras se pueden ordenar los datos de forma ascendente o descente, se ser el caso se puede filtrar los datos([ver sección 4](#4-filtros-personalizados)).

![Screenshot](../img/det_devices.jpg)

!!! Note
    De requerir registrar un nuevo `device` contactese con el administrador del sistema.

## 4. Filtros Personalizados

Todas las columnas cuentan con filtros personalizados.

![Screenshot](../img/filter_devices.jpg)