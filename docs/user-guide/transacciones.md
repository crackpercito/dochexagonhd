# Transacciones

![Screenshot](../img/menu_transacciones.jpg)

## 1. Detalle

Lista todas las `transacciones` de todos los sites, haciendo click en cada una de las cabeceras se pueden ordenar los datos de forma ascendente o descente, se ser el caso se puede filtrar los datos([ver sección 3](#3-filtros-personalizados)).

![Screenshot](../img/det_transacciones.jpg)

## 2. `Cancelar`

Esta opción permite regresar a la vista de [`componentes`](/user-guide/componentes/)

## 3. Filtros Personalizados

Todas las columnas cuentan con filtros personalizados.

![Screenshot](../img/filter_transacciones.jpg)