# Mis Tickets

![Screenshot](../img/menu_tickets.jpg)

## 1. Detalle

Lista los tickets de atenciones del usuario actual ordernado por fecha de inicio(`StartAt`), haciendo click en cada una de las cabeceras se pueden ordenar los datos de forma ascendente o descente, se ser el caso se puede filtrar los datos([ver sección 5](#5-filtros-personalizados)).


![Screenshot](../img/my_tickets.jpg)

## 2. `Nuevo` - Registrar Ticket

Esta opción permite registar una nueva atención.

![Screenshot](../img/new_ticket.jpg)

### A. `Atendido por`

Registra la persona que realizó la atención (por defecto se asignará al usuario actual), tambien se puede agregar a otras personas que participaron en la atención, para esto solo se debe digitar el nombre de esta y apareceran las coicidencias, por último seleccionar a quien se quiere agregar.

![Screenshot](../img/tk_atendio_por.jpg)

### B. `Site`

Representa el `proyecto`/`faena` a la que pertence la atención, al seleccionarse se filtran automaticamente los equipos pertenecientes a esta.

### C. `Equipo`

Aquí se asigna el equipo que fue atendido.

![Screenshot](../img/tk_equipo.jpg)

### D. `Reportado por`

Se indica que persona por parte del cliente reportó el incidente o realizó el requerimiento.

### E. `Descripcion`

Descripción corta y significativa de la atención

### F. `Detalle`

Descripción detallada de la atención del equipo.

### G. `Categoria`

- **Software:** Asignado por defecto a técnicos de operaciones
- **Hardware:** Asignado por defecta a Ingenieros de operaciones.


### H. `Estado`

- **Cerrado**
- **Pendiente**
- **En progreso**



### I. `Prioridad`

#### SLA Prioridad 1 Critical 

El nivel crítico corresponde a problemas/solicitudes que puedan significar una atención inmediata y que pueden impactar en el normal funcionamiento del sistema. Se incluyen los siguientes ítems como por ejemplo:

- Caída de sistema JMineOPS
- Caída de plataforma de reportes
- Caída de la red inalámbrica
  
#### SLA Prioridad 2 High

El nivel alto corresponde a problemas/solicitudes que puedan significar una atención casi inmediata o que necesiten programarse con alta prioridad y que eventualmente pueden impactar en el normal funcionamiento del sistema. Se incluyen los siguientes ítems como ejemplo:

- Modificación, eliminación o creación de registros de tabla de configuraciones del sistema
- Modificación de formulación de consultas SQL en todo nivel de sistema (JMineOPS/JView)
- Instalación de hardware en nuevos equipos mina
- Creación o eliminación equipos en el SW de monitoreo de la red.
- Caída de plataforma de reportes sobre status de la red

#### SLA Prioridad 3 Medium 

El nivel medio corresponde a problemas/solicitudes que puedan significar una atención oportuna pero que no impide el normal funcionamiento del sistema en tiempo real. Se incluyen los siguientes ítems como ejemplo:

- Creación o eliminación de equipos mina en JMineOPS
- Sumarización de información en la base de datos de turnos finalizados
- Traslado de hardware de un equipo a otro

#### SLA Prioridad 4 Low

El nivel bajo corresponde a problemas/solicitudes que no impide bajo ningún punto el mantenimiento y funcionamiento normal de la operación en tiempo real. Se incluyen los siguientes ítems como ejemplo:

- Creación o eliminación de cuenta de usuarios en JMineOPS o JView
- Cualquier modificación de dato (origen, destino, tonelaje, equipo, etc.) a algún ciclo de producción de turnos finalizados
- Cambio de estados a algún (os) equipo (s) mina en turnos finalizados
- Modificación, eliminación o creación de cargas/descargas en el sistema en turnos finalizados. Creación de reportes básicos en Excel u otra herramienta sobre el status de la red.
- Atención de equipos de campo (radios, switch, cables, conectores y antenas de comunicaciones)
- Configuración de los componentes de red para equipos de campo
- Cambio de hardware en mal estado o mal funcionamiento en equipos de campo
- Atención de consultas y apoyo a despachador mina

### J. `Tipo`

- **Incidente**
- **Requerimiento**
- **Accidente**


### K. `Solución`

- **Soporte**
- **Desarrollo**
- **Mantenimiento  Preventivo**
- **Desistanlación Sistema**
- **Instalación Nueva**

### L. `Inicio`

Fecha y hora del inicio de la atención.

### M. `Fin`

Fecha y hora del fin de la atención.

### N. `Tags`

Palabras clave para futuras busquedas.

### O. `Ampliar Detalles`

Aqui se puede ingresar mayor detalle de la atención.

![Screenshot](../img/tk_amp_detalles.jpg)

## 3. `Editar` - Editar Ticket

Primero debemos seleccionar la casilla del ticket que deseamos modificar, luego hacemos click en `Editar`.

![Screenshot](../img/tk_editar.jpg)

Luego tenemos las mismas opciones que al `Registrar Ticket` y adicionalmente la opcion de `Archivos` la cual permite ajuntar archivos a un `Ticket` ya creado.

![Screenshot](../img/tk_update.jpg)

 Esta opción presenta las siguiente funcionalidad:

- i. `Descripción`, descripcion corta y significativa del archivo que se va adjuntar al `ticket`.
 - ii. `File`, aquí podemos seleccionar el archivo que deseamos adjuntar al `ticket`.  
 - iii. `Archivos disponibles`, lista los archivos asociados al `ticket` que se está `editando`, ordernado por la fecha que se adjunto(`UploadtAt`), haciendo click en cada una de las cabeceras se pueden ordenar los datos de forma ascendente o descente, se ser el caso se puede filtrar los datos([ver sección 5](#5-filtros-personalizados)).
 - iv. `Eliminar` - Eliminar Archivo, primero debemos seleccionar una o varias casillas de los archivos que deseamos eliminar, luego hacemos click en `Eliminar` y confirmamos la accion.
![Screenshot](../img/tk_delete_file.jpg)
![Screenshot](../img/tk_file_delete_confirm.jpg)
- v. `Descargar` - Descargar Archivo, rimero debemos seleccionar la casilla del archivo que deseamos descargar, luego hacemos click en `Descargar`.


## 4. `Eliminar` - Eliminar Tickets

Primero debemos seleccionar una o varias casillas de los tickets que deseamos eliminar, luego hacemos click en `Eliminar`.

![Screenshot](../img/tk_delete.jpg)

Confirmamos o cancelamos la eliminación.

![Screenshot](../img/tk_delete_ok.jpg)

## 5. Filtros Personalizados

Todas las columnas cuentan con filtros personalizados.

![Screenshot](../img/filter.jpg)
