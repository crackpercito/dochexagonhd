# Todos los Tickets

![Screenshot](../img/all_tickets.jpg)

Aqui podemos visualizar todos los tickets ingresados por todos los usuarios y todos los sites, esto no sirve como base de conocimiento y poder buscar problemas similares y cual fue su solución.

![Screenshot](../img/my_tickets_d.jpg)

!!! Note
    De igual manera todas las columnas se puede ordenar haciendo click en las cabeceras, todas las grillas cuentan con filtros personalizados.

## A. Ver Ticket

Previamente debemos haber seleccionado la casilla del tickect que deseamos visualizar.

![Screenshot](../img/tk_view.jpg)

